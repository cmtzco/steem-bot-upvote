# Steemit Upvote Bot


## Installation

```
#!bash

    git clone https://mrMuffins@bitbucket.org/mrMuffins/steem-bot-upvote.git

```
```
#!bash

    cd steem-bot-upvote

```
```
#!bash

    sudo ./install.sh

```
  
This is where you will set the account, posting key, and users that you wish to upvote
```
#!bash

    nano steemvars.py

```
```
#!bash

    ./run.sh
```